package wordchain.manager;

import java.io.DataInputStream;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Map;
import java.util.Scanner;
import java.util.TreeMap;

/**
 *
 * @author Joasia
 */
public class Dictionary {

    private static Map<Character, ArrayList> words = new TreeMap<>();
    private final static String dictPath = "pl.txt";

    public static String getStartingFromLetter(char letter) {
        letter = String.valueOf(letter).toLowerCase().charAt(0);
        String ret_val = null;
        try {
            ret_val = (String) words.get(Character.valueOf(letter)).get(0);
            markAsUsed(ret_val);
        } catch (IllegalAccessError e) {
            System.out.println("No words left in dictionary starting from: " + String.valueOf(letter));
        } finally {
            return ret_val;
        }
    }

    public static void markAsUsed(String word) {
        words.get(Character.valueOf(word.charAt(0))).remove(word);
    }

    public static void initDicts() throws FileNotFoundException, IOException {
        FileInputStream fstream = new FileInputStream(dictPath);
        DataInputStream in = new DataInputStream(fstream);
        Scanner scanner;
        scanner = new Scanner(in, "windows-1250");
        while (scanner.hasNext()) {
            String word = scanner.nextLine().toLowerCase();
            if (!words.containsKey(Character.valueOf(word.charAt(0)))) {
                words.put(Character.valueOf(word.charAt(0)), new ArrayList<String>());
            }
            words.get(Character.valueOf(word.charAt(0))).add(word);
        }
    }
}
