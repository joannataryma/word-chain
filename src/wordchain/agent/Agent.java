package wordchain.agent;

import wordchain.utils.Utils;
import com.rabbitmq.client.Channel;
import com.rabbitmq.client.Connection;
import com.rabbitmq.client.ConnectionFactory;
import com.rabbitmq.client.ConsumerCancelledException;
import com.rabbitmq.client.QueueingConsumer;
import com.rabbitmq.client.ShutdownSignalException;
import java.io.IOException;
import java.util.UUID;
import java.util.logging.Level;
import java.util.logging.Logger;
import java.net.ConnectException;

public class Agent {

    private UUID id;
    private String last_word = "a";
    private ConnectionFactory connectionFactory;
    private Channel recChannelPriv, sendChannel;

    public Agent() {
        this.id = UUID.randomUUID();
    }

    private void run() {

        connectionFactory = new ConnectionFactory();
        connectionFactory.setHost("localhost");
        Connection recConnection, sendConnection;
        try {
            recConnection = connectionFactory.newConnection();
            recChannelPriv = recConnection.createChannel();
            sendConnection = connectionFactory.newConnection();
            sendChannel = sendConnection.createChannel();
            QueueingConsumer consumerPriv = new QueueingConsumer(recChannelPriv);
            String initMessage = id.toString() + ":" + Utils.AGENT_START_MSG;

            //Init agent msg to manager
            sendChannel.queueDeclare(Utils.QUEUE_NAME_MGR, false, false,
                    false, null);
            sendChannel.basicPublish("", Utils.QUEUE_NAME_MGR, null,
                    initMessage.getBytes());

            //Receiving on private queue
            recChannelPriv.queueDeclare(Utils.QUEUE_NAME + id.toString(),
                    false, false, false, null);
            recChannelPriv.basicConsume(Utils.QUEUE_NAME + id.toString(),
                    true, consumerPriv);


            while (true) {
                QueueingConsumer.Delivery deliveryPriv = consumerPriv.nextDelivery();
                String rec_message_priv = new String(deliveryPriv.getBody());
                receivePriv(rec_message_priv);
            }
        } catch (ConnectException ex) {
            Logger.getLogger(Agent.class.getName()).log(Level.SEVERE,
                    "Could not connect!");
        } catch (InterruptedException |
                ShutdownSignalException |
                ConsumerCancelledException |
                IOException ex) {
            Logger.getLogger(Agent.class.getName()).log(Level.SEVERE,
                    "Oops! Something wrong happened...");
        }
    }

    private void receivePriv(String msg) throws IOException {
        if (msg.equals(Utils.TOKEN_GAINED_MSG)) {
            //request a word from dict
            send_req_to_dict(last_word.charAt(last_word.length() - 1));
        } else if (msg.split(":")[0].equals(Utils.LAST_WORD_TAG)) {
            //other playing agent sent word
            System.out.println("Last: " + msg.split(":")[1]);
            String word = msg.split(":")[1];
            this.last_word = word;
        } else {
            send_word(msg);
        }
    }

    private void send_req_to_dict(char letter) throws IOException {
        sendChannel.queueDeclare(Utils.QUEUE_NAME_MGR, false, false, false, null);
        String message = id.toString() + ":" + Utils.DICT_REQ_MSG
                + ":" + String.valueOf(letter);
        sendChannel.basicPublish("", Utils.QUEUE_NAME_MGR, null, message.getBytes());
    }

    private void send_word(String msg) throws IOException {
        sendChannel.queueDeclare(Utils.QUEUE_NAME_MGR, false, false, false, null);
        String message = id.toString() + ":" + msg;
        sendChannel.basicPublish("", Utils.QUEUE_NAME_MGR, null, message.getBytes());
    }

    public static void main(String args[]) {
        Agent agent = new Agent();
        agent.run();
    }
}
