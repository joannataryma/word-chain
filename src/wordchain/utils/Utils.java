package wordchain.utils;

import com.rabbitmq.client.Connection;
import com.rabbitmq.client.ConnectionFactory;
import java.io.IOException;

public class Utils {
    public final static String QUEUE_NAME = "wordgame.";
    public final static String QUEUE_NAME_PUB = "wordgame.all";
    public final static String QUEUE_NAME_MGR = "wordgame.manager";
    public final static String AGENT_START_MSG = "Hello";
    public final static String TOKEN_GAINED_MSG = "Go!";
    public final static String DICT_REQ_MSG = "Word, pls";
    public final static String LAST_WORD_TAG = "Last";
    
    public static Connection createConnection() throws IOException {
            ConnectionFactory factory = new ConnectionFactory();
            factory.setHost("localhost");
            Connection connection = factory.newConnection();
            return connection;
    }
}
