package wordchain.manager;

import java.io.FileNotFoundException;
import java.io.IOException;
import java.util.UUID;
import org.junit.BeforeClass;
import org.junit.Test;
import static org.junit.Assert.*;

/**
 *
 * @author Joasia
 */
public class DictionaryTest {

    public DictionaryTest() { }

    @BeforeClass
    public static void setUpClass() throws FileNotFoundException, IOException {
        Dictionary.initDicts();
    }

    /**
     * Test of get_starting_from_letter method, of class Dictionary.
     */
    @Test
    public void testGetStartingFromLetter() {
        char letter = 'a';
        String result = Dictionary.getStartingFromLetter(letter);
        assertEquals(result.charAt(0), letter);
    }

    @Test
    public void testGetStartingFromLetterMultiple() {
        //Only 3 words starting from 'x' in polish :)
        char letter = 'x';
        int x = 0;
        String result = null;
        do {
            x++;
            result = Dictionary.getStartingFromLetter(letter);
        } while (result != null);
        assertTrue(x == 4);
    }

    /**
     * Test of mark_as_used method, of class Dictionary.
     */
    @Test
    public void testMarkAsUsed() {
        String word = Dictionary.getStartingFromLetter('a');
        Dictionary.markAsUsed(word);
        String expected = Dictionary.getStartingFromLetter('a');
        System.out.println(UUID.randomUUID().toString().length());
        assertNotSame(expected, word);
    }
}
